package com.example.courselivecode.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
@ToString
public class Organization {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String name;

    @OneToMany(mappedBy = "organization", fetch = FetchType.EAGER)
    @JsonIgnore
    List<Task> tasks = new ArrayList<>();

    @OneToMany(mappedBy = "organization", fetch = FetchType.EAGER)
    @JsonIgnore
    List<User> users = new ArrayList<>();
}
