package com.example.courselivecode.controller;

import com.example.courselivecode.dto.UserRequest;
import com.example.courselivecode.dto.UserResponse;
import com.example.courselivecode.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    @GetMapping("/{userId}")
    public UserResponse getUserById(@PathVariable Long userId){
        return userService.getUserById(userId);
    }

    @GetMapping("/all")
    public List<UserResponse> getAllUsers(){
        return userService.getAllUsers();
    }

    @PostMapping("/{organizationId}/create")
    public UserResponse create(@PathVariable Long organizationId, @RequestBody UserRequest request){
        return userService.create(organizationId, request);
    }

    @PutMapping("/{userId}")
    public UserResponse update(@PathVariable Long userId, @RequestBody UserRequest request){
        return userService.update(userId, request);
    }

    @DeleteMapping("/{taskId}/user/{userId}")
    public void deleteUserById(@PathVariable Long taskId, @PathVariable Long userId){
        userService.deleteUserById(taskId, userId);
    }
}
