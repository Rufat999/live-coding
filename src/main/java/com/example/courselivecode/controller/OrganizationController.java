package com.example.courselivecode.controller;

import com.example.courselivecode.dto.OrganizationRequest;
import com.example.courselivecode.dto.OrganizationResponse;
import com.example.courselivecode.entity.Organization;
import com.example.courselivecode.service.OrganizationService;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/organization")
public class OrganizationController {

    private final OrganizationService organizationService;

    @GetMapping("/{organizationId}")
    public OrganizationResponse getOrganizationById(@PathVariable Long organizationId){
        return organizationService.getOrganizationById(organizationId);
    }

    @GetMapping("/all")
    public List<OrganizationResponse> getAllOrganizations(){
        return organizationService.getAllOrganizations();
    }

    @PostMapping("/create")
    public OrganizationResponse create(@RequestBody OrganizationRequest request){
        return organizationService.create(request);
    }

    @PutMapping("/{organizationId}/update")
    public OrganizationResponse update(@PathVariable Long organizationId, @RequestBody OrganizationRequest request){
        return organizationService.update(organizationId, request);
    }

    @DeleteMapping("/{organizationId}")
    public void deleteById(@PathVariable Long organizationId){
        organizationService.deleteById(organizationId);
    }
}
