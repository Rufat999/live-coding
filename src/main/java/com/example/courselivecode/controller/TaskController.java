package com.example.courselivecode.controller;

import com.example.courselivecode.dto.OrganizationRequest;
import com.example.courselivecode.dto.TaskRequest;
import com.example.courselivecode.dto.TaskResponse;
import com.example.courselivecode.service.TaskService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/task")
public class TaskController {

    private final TaskService taskService;

    @GetMapping("/{taskId}")
    public TaskResponse getTaskById(@PathVariable Long taskId){
        return taskService.getTaskById(taskId);
    }

    @GetMapping("/all")
    public List<TaskResponse> getAllTasks(){
        return taskService.getAllTasks();
    }

    @PostMapping("/{organizationId}/create")
    public TaskResponse create(@PathVariable Long organizationId, @RequestBody TaskRequest request){
        return taskService.create(organizationId, request);
    }

    @PostMapping("/{taskId}/{userId}")
    public void createRelation(@PathVariable Long taskId, @PathVariable Long userId){
        taskService.createRelation(taskId, userId);
    }

    @PutMapping("/{taskId}")
    public TaskResponse update(@PathVariable Long taskId, @RequestBody TaskRequest request){
        return taskService.update(taskId, request);
    }

    @DeleteMapping("/{taskId}")
    public void deleteTask(@PathVariable Long taskId){
        taskService.deleteTask(taskId);
    }

    @DeleteMapping("/all")
    public void deleteAllTasks(){
        taskService.deleteAllTasks();
    }
}
