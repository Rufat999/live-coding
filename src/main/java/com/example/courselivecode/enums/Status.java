package com.example.courselivecode.enums;


public enum Status {
    ACTIVE, TODO, DONE
}
