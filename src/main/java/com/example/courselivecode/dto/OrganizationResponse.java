package com.example.courselivecode.dto;

import com.example.courselivecode.entity.Task;
import com.example.courselivecode.entity.User;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
@ToString
public class OrganizationResponse {
    Long id;
    String name;
    List<Task> tasks = new ArrayList<>();
    List<User> users = new ArrayList<>();
}
