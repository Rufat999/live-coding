package com.example.courselivecode.dto;

import com.example.courselivecode.entity.Organization;
import com.example.courselivecode.entity.User;
import com.example.courselivecode.enums.Status;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
@ToString
public class TaskResponse {
    Long id;
    Date deadLine;
    Status status;
    String description;
    List<User> users = new ArrayList<>();
}