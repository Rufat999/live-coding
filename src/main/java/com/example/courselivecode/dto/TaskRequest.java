package com.example.courselivecode.dto;

import com.example.courselivecode.enums.Status;
import jakarta.persistence.Enumerated;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
@ToString
public class TaskRequest {
    Date deadLine;
    Status status;
    String description;
}
