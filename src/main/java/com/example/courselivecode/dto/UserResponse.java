package com.example.courselivecode.dto;

import com.example.courselivecode.entity.Organization;
import com.example.courselivecode.entity.Task;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
@ToString
public class UserResponse {

    Long id;
    String name;
    String surname;
    String email;
    String password;
    private List<Task> tasks = new ArrayList<>();
}
