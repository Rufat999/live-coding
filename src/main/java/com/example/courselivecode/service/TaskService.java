package com.example.courselivecode.service;

import com.example.courselivecode.dto.TaskRequest;
import com.example.courselivecode.dto.TaskResponse;
import com.example.courselivecode.entity.Organization;
import com.example.courselivecode.entity.Task;
import com.example.courselivecode.entity.User;
import com.example.courselivecode.repository.OrganizationRepository;
import com.example.courselivecode.repository.TaskRepository;
import com.example.courselivecode.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TaskService {

    private final TaskRepository taskRepository;
    private final OrganizationRepository organizationRepository;
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;

    public TaskResponse getTaskById(Long taskId) {
        Task task = taskRepository.findById(taskId)
                .orElseThrow(() -> new RuntimeException("Task " + taskId + " not found!"));
        TaskResponse taskResponse = modelMapper.map(task, TaskResponse.class);
        return taskResponse;
    }

    public List<TaskResponse> getAllTasks() {
        List<Task> task = taskRepository.findAll();
        List<TaskResponse> taskResponseList = task.stream().map(task1 -> modelMapper.map(task1, TaskResponse.class)).collect(Collectors.toList());
        return taskResponseList;
    }

    public TaskResponse create(Long organizationId, TaskRequest request) {
        Organization organization = organizationRepository.findById(organizationId)
                .orElseThrow(() -> new RuntimeException("Organization " + organizationId + " not found!"));
        Task task = modelMapper.map(request, Task.class);
        organization.getTasks().add(task);
        task.setOrganization(organization);
        taskRepository.save(task);
        TaskResponse taskResponse = modelMapper.map(task, TaskResponse.class);
        return taskResponse;
    }

    public void createRelation(Long taskId, Long userId) {
        Task task = taskRepository.findById(taskId).get();
        User user = userRepository.findById(userId).get();
        task.getUsers().add(user);
        taskRepository.save(task);
    }

    public TaskResponse update(Long taskId, TaskRequest request) {
        Task task = taskRepository.findById(taskId)
                .orElseThrow(() -> new RuntimeException("Task " + taskId + " not found!"));
        task.setDeadLine(request.getDeadLine());
        task.setStatus(request.getStatus());
        task.setDescription(request.getDescription());
        taskRepository.save(task);
        TaskResponse taskResponse = modelMapper.map(task, TaskResponse.class);
        return taskResponse;
    }

    public void deleteTask(Long taskId) {
        taskRepository.deleteById(taskId);
    }

    public void deleteAllTasks() {
        taskRepository.deleteAll();
    }
}

