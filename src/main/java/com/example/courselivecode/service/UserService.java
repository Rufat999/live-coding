package com.example.courselivecode.service;

import com.example.courselivecode.dto.UserRequest;
import com.example.courselivecode.dto.UserResponse;
import com.example.courselivecode.entity.Organization;
import com.example.courselivecode.entity.Task;
import com.example.courselivecode.entity.User;
import com.example.courselivecode.repository.OrganizationRepository;
import com.example.courselivecode.repository.TaskRepository;
import com.example.courselivecode.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final TaskRepository taskRepository;
    private final OrganizationRepository organizationRepository;
    private final ModelMapper modelMapper;

    public UserResponse getUserById(Long userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new RuntimeException("User " + userId + " not found!"));
        UserResponse userResponse = modelMapper.map(user, UserResponse.class);
        return userResponse;
    }

    public List<UserResponse> getAllUsers() {
        List<User> users = userRepository.findAll();

        List<UserResponse> userResponseList = users.stream().map(user -> modelMapper.map(user, UserResponse.class)).collect(Collectors.toList());
        return userResponseList;
    }

    public UserResponse create(Long organizationId, UserRequest request) {
        Organization organization = organizationRepository.findById(organizationId)
                .orElseThrow(() -> new RuntimeException("Organization " + organizationId + " not found!"));
        User user = modelMapper.map(request, User.class);
        organization.getUsers().add(user);
        user.setOrganization(organization);
        userRepository.save(user);
        UserResponse userResponse = modelMapper.map(user, UserResponse.class);
        return userResponse;
    }

    public void deleteUserById(Long taskId, Long userId) {
        Task task = taskRepository.findById(taskId)
                .orElseThrow(() -> new RuntimeException("Task " + taskId + " not found!"));
        User user = task.getUsers().stream().filter(u -> Objects.equals(u.getId(), userId)).findFirst().orElse(null);
        if (user != null) {
            task.getUsers().remove(user);
            user.getTasks().remove(task);
        }
        taskRepository.save(task);
    }

    public UserResponse update(Long userId, UserRequest request) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new RuntimeException("User " + userId + " not found!"));
        user.setName(request.getName());
        user.setSurname(request.getSurname());
        user.setEmail(request.getEmail());
        user.setPassword(request.getPassword());
        userRepository.save(user);
        UserResponse userResponse = modelMapper.map(user, UserResponse.class);
        return userResponse;
    }
}
