package com.example.courselivecode.service;

import com.example.courselivecode.dto.OrganizationRequest;
import com.example.courselivecode.dto.OrganizationResponse;
import com.example.courselivecode.entity.Organization;
import com.example.courselivecode.repository.OrganizationRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrganizationService {

    private final OrganizationRepository organizationRepository;
    private final ModelMapper modelMapper;

    public OrganizationResponse getOrganizationById(Long organizationId) {
        Organization organization = organizationRepository.findById(organizationId)
                .orElseThrow(() -> new RuntimeException("Organization " + organizationId + " not found!"));
        OrganizationResponse organizationResponse = modelMapper.map(organization, OrganizationResponse.class);
        return organizationResponse;
    }

    public List<OrganizationResponse> getAllOrganizations() {
        List<Organization> organizationList = organizationRepository.findAll();
        List<OrganizationResponse> organizationResponseList = organizationList.stream().map(organization -> modelMapper.map(organization, OrganizationResponse.class)).collect(Collectors.toList());
        return organizationResponseList;
    }

    public OrganizationResponse create(OrganizationRequest request) {
        Organization organization = modelMapper.map(request, Organization.class);
        organizationRepository.save(organization);
        OrganizationResponse organizationResponse = modelMapper.map(organization, OrganizationResponse.class);
        return organizationResponse;
    }

    public OrganizationResponse update(Long organizationId, OrganizationRequest request) {
        Organization organization = organizationRepository.findById(organizationId)
                .orElseThrow(() -> new RuntimeException("Organization " + organizationId + " not found!"));
        organization.setName(request.getName());
        organizationRepository.save(organization);
        OrganizationResponse organizationResponse = modelMapper.map(organization, OrganizationResponse.class);
        return organizationResponse;
    }

    public void deleteById(Long organizationId) {
        organizationRepository.deleteById(organizationId);
    }
}
