package com.example.courselivecode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CourseLiveCodeApplication {

    public static void main(String[] args) {
        SpringApplication.run(CourseLiveCodeApplication.class, args);
    }

}
