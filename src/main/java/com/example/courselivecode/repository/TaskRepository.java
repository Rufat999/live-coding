package com.example.courselivecode.repository;

import com.example.courselivecode.entity.Task;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskRepository extends JpaRepository<Task, Long> {
}
