package com.example.courselivecode.repository;

import com.example.courselivecode.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
}
